Role "Docker-ce"
=========

Used to install docker-ce and docker-compose

Molecule test: 

Example Playbook
----------------

```
- name: Install docker-ce and docker-compose
  hosts: all
  become: true
  
  roles:
    - docker-ce
```

Include this role using an Ansible Galaxy requirements.yml file. E.g.:
```
- src: git+https://gitlab.com/ansible-achadwick/roles/docker-ce.git
  scm: git
  version: "0.1.0"  # quoted, so YAML doesn't parse this as a floating-point value
  name: docker-ce
```
And running:
```
ansible-galaxy role install -r requirements.yml
```

License
-------

BSD
